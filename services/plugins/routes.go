package plugins

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/config"

	"log"
	"net/http"
	"net/url"

	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/kong"
)

type PluginInfo struct {
	Name  string `json:"name"`
	Route string `json:"route"`
	URL   string `json:"url"`
}

func AddPluginService(rg *gin.RouterGroup) {
	pluginsGroup := rg.Group("/plugins")

	pluginsGroup.GET("/", func(ctx *gin.Context) {
		ListPlugins(ctx)
	})
}

func ListPlugins(ctx *gin.Context) {
	k := config.PluginDiscoveryConfig.Kong

	scheme := k.Scheme
	if scheme == "" {
		scheme = "http"
	}

	kongHost := k.Host
	if kongHost == "" {
		log.Fatal("missing kong host (expected to be passed via ENV['KONG_HOST'])")
	}

	pluginTag := k.Scheme
	if pluginTag == "" {
		pluginTag = "pcm-plugin"
	}

	baseURL := &url.URL{
		Scheme: "http",
		Host:   kongHost,
	}

	client := kong.NewClient(baseURL)
	kongRoutes, _, err := client.ListRoute(pluginTag)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, "Error sending request")
		log.Printf("%s\t%s\t%v\t%s", "GET", ctx.Request.Host+ctx.Request.URL.Path, http.StatusInternalServerError, err)
	}

	kongServices, _, err := client.ListServices()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, "Error sending request")
		log.Printf("%s\t%s\t%v\t%s", "GET", ctx.Request.Host+ctx.Request.URL.Path, http.StatusInternalServerError, err)
	}

	plugins := make([]PluginInfo, 0)
	for _, route := range kongRoutes {
		if route.Service != nil {
			plugin := PluginInfo{
				Route: route.Name,
				URL:   route.Paths[0],
			}

			for _, service := range kongServices {
				if route.Service.ID == service.ID {
					plugin.Name = service.Name
				}
			}
			plugins = append(plugins, plugin)
		}
	}

	ctx.JSON(200, plugins)

}
