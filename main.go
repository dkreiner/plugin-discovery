package main

import (
	logPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/config"
	"log"
)

var env *common.Environment

func main() {
	env = common.GetEnvironment()
	if err := config.Load(); err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	currentConf := config.PluginDiscoveryConfig

	logger, err := logPkg.New(currentConf.LogLevel, currentConf.IsDev, nil)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	env.SetLogger(logger)

	api.Listen(env)
}
