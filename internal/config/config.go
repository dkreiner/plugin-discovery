package config

import (
	configPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
)

type pluginDiscoveryConfig struct {
	configPkg.BaseConfig `mapstructure:",squash"`
	Topic                string `mapstructure:"topic"`

	Kong struct {
		Scheme string `mapstructure:"scheme"`
		Host   string `mapstructure:"host"`
		Tag    string `mapstructure:"tag"`
	} `mapstructure:"kong"`
}

var PluginDiscoveryConfig pluginDiscoveryConfig

func Load() error {
	return configPkg.LoadConfig("PLUGINDISCOVERY", &PluginDiscoveryConfig, getDefaults())
}

func getDefaults() map[string]any {
	return map[string]any{
		"isDev": false,
	}
}
