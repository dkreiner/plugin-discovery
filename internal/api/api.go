package api

import (
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/common"
	"sync"
)

func Listen(env *common.Environment) {
	var wg sync.WaitGroup

	wg.Add(1)
	go startMessaging(&wg, env)
	go startRest(&wg, env)

	wg.Wait()
}
