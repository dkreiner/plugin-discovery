package api

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/services/plugins"
	"sync"
)

func startRest(wg *sync.WaitGroup, env *common.Environment) {
	defer wg.Done()

	start := server.New(env)
	start.Add(plugins.AddPluginService)

	start.Run(config.PluginDiscoveryConfig.Port)
}
