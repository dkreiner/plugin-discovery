package api

import (
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/internal/config"
	"os"
	"sync"
)

func startMessaging(group *sync.WaitGroup, env *common.Environment) {
	logger := env.GetLogger()
	defer group.Done()
	logger.Info("start messaging!")

	client, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, config.PluginDiscoveryConfig.Topic)
	if err != nil {
		logger.Error(err, "Failed to initialize CloudEvent Provider")
		os.Exit(1)
	}

	defer func() {
		if err := client.Close(); err != nil {
			logger.Error(err, "closing cloudEventProvider Client failed!")
		}
	}()

	err = client.Sub(handleMessage)
	if err != nil {
		logger.Error(err, "cloudEventProvider.Sub failed")
	}
}

func handleMessage(event event.Event) {
	//TODO currently unused
}
