package common

import (
	logPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
)

type Environment struct {
	logger    *logPkg.Logger
	isHealthy bool
}

var env *Environment

func init() {
	env = new(Environment)
}

func GetEnvironment() *Environment { return env }

func (e *Environment) SetLogger(logger *logPkg.Logger) {
	e.logger = logger
}

func (e *Environment) GetLogger() *logPkg.Logger { return e.logger }

func (e *Environment) SetHealthy(isHealthy bool) {
	e.isHealthy = isHealthy
}

func (e *Environment) IsHealthy() bool {
	return e.isHealthy
}
